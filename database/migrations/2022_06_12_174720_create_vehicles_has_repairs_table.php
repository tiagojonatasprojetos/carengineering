<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesHasRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('vehicles_has_repair', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->integer('km_entrada');
            $table->string('status');
            $table->string('placa');
            $table->string('reclamacao');
            $table->string('tecnico');
            $table->string('obs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles_has_repairs');
    }
}
