<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class inventories extends Model
{
    use HasFactory;

    protected $table = 'inventories';
    protected $fillable = ['id',
                           'nome',
                           'mmarca',
                           'aplicacao',
                           'cod_peca'
                        ];
}
