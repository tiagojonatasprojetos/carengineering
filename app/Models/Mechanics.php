<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mechanics extends Model
{
    use HasFactory;

    protected $table = 'mechanics';
    protected $fillable = ['nome',
                           'telefone',
                           'email',
                           'formacao',
                           'comissao',
                           'especialidade',
                           'ultimo_curso',
                           'dat_ultimo_curso'
                        ];
}
