<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\MechanicsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Rotas CRUD do cliente

Route::get('/customers', [CustomersController::class, "index"]);
Route::post('/customers', [CustomersController::class, "create"]);
Route::put('/customers/{id}', [CustomersController::class, "update"]);
Route::delete('/customers/{id}', [CustomersController::class, "destroy"]);

// Rotas CRUD de  Mecanicos

Route::get('/mechanics', [MechanicsController::class, "index"]);
Route::post('/mechanics', [MechanicsController::class, "create"]);
Route::put('/mechanics/{id}', [MechanicsController::class, "update"]);
Route::delete('/mechanics/{id}', [MechanicsController::class, "destroy"]);




