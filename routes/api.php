<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\MechanicsController;
use App\Http\Controllers\vehiclesRepairController;
use App\Http\Controllers\InventoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Rotas CRUD Customers

Route::get('/customers', [CustomersController::class, "index"]);
Route::post('/customers', [CustomersController::class, "create"]);
Route::put('/customers/{id}', [CustomersController::class, "update"]);
Route::delete('/customers/{id}', [CustomersController::class, "destroy"]);

// Rotas CRUD de  Mecanicos

Route::get('/mechanics', [MechanicsController::class, "index"]);
Route::post('/mechanics', [MechanicsController::class, "create"]);
Route::put('/mechanics/{id}', [MechanicsController::class, "update"]);
Route::delete('/mechanics/{id}', [MechanicsController::class, "destroy"]);

// Rotas Para Veiculos

Route::get('/vehicles', [vehiclesRepairController::class, "index"]);
Route::post('/vehicles', [vehiclesRepairController::class, "create"]);
Route::put('/vehicles/{id}', [vehiclesRepairController::class, "update"]);
Route::delete('/vehicles/{id}', [vehiclesRepairController::class, "destroy"]);

// Rotas para estoque
Route::get('/inventory', [InventoryController::class, "index"]);
Route::post('/inventory', [InventoryController::class, "create"]);
Route::put('/inventory/{id}', [InventoryController::class, "update"]);
Route::delete('/inventory/{id}', [InventoryController::class, "destroy"]);



